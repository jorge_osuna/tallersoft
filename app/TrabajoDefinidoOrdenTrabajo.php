<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\OrdenTrabajo;
use App\TrabajoDefinido;
class TrabajoDefinidoOrdenTrabajo extends Model
{
    //
    protected $fillable = [
        'idRefacciones','idOrdenTrabajo'
    ];

    public function trabajoDefinido(){
        return $this->hasMany(TrabajoDefinido::class);
    }
    public function ordenT(){
        return $this->hasMany(OrdenTrabajo::class);
    }
}
