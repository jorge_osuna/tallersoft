<?php

namespace App;

use App\ProductoReparacion;
use App\OrdenTrabajo;
use Illuminate\Database\Eloquent\Model;

class ProductoReparacionOrdenTrabajo extends Model
{
    //
    protected $fillable = [
        'idProductoReparacion','idOrdenTrabajo'
    ];

    public function productoReparacion(){
        return $this->hasMany(ProductoReparacion::class);
    }
    public function ordenT(){
        return $this->hasMany(OrdenTrabajo::class);
    }
}
