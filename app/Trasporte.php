<?php

namespace App;

use App\Clientes;
use Illuminate\Database\Eloquent\Model;

class Trasporte extends Model
{
    //
    protected $fillable = [
        'idCliente','marca','modelo','ano','no_eco','serie','descripcion'
    ];
    
    public function cliente(){
        return $this->belongsToMany(Clientes::class);
    }
}
