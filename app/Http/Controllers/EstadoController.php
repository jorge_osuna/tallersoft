<?php

namespace App\Http\Controllers;

use App\EstadoOrdenTrabajo;
use Illuminate\Http\Request;

class EstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // \dd($request);
        $orden=EstadoOrdenTrabajo::latest()->where('idOrden',$request->idOrden)->first();
        $esdato=new EstadoOrdenTrabajo;
        $esdato->idOrden=$request->idOrden;
        switch ($orden->estado) {
            case 'REGISTRADO':
                $esdato->estado='EN_COLA';
                break;
            case 'EN_COLA':
                $esdato->estado='CONFIRMACION_R/P';
                break;
            case 'CONFIRACION_R/P':
                $esdato->estado='EN_ESPERA_R/P';
                break;
            case 'EN_ESPERA_R/P':
                $esdato->estado='R/P_EN_TALLER';
                break;
            case 'R/P_EN_TALLER':
                $esdato->estado='EN_TRABJO';
                break;
            case 'EN_TRABJO':
                $esdato->estado='TERMINADO';
                break;
            case 'TERMINADO':
                $esdato->estado='PAGADO_FACTURADO';
                break;
            case 'PAGADO_FACTURADO':
                $esdato->estado='FINALIZADO';
                break;
            case 'FINALIZADO':
                # code...
                break;
            
        }
        $esdato->save();
        return \redirect()->route('ordentrabajo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
