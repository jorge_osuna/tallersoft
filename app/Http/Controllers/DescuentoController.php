<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\Descuento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DescuentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=DB::table('clientes')
                ->join('descuentos','descuentos.idCliente','=','clientes.id')
                ->select('clientes.id','clientes.no_cliente','clientes.nombre',
                'clientes.rfc','descuentos.tipo_descuento')->get();
        return view('descuento.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $clientes=Clientes::all();
        $descuentos=Descuento::all();
        $datos=[
            "clientes"=>$clientes,
            "descuentos"=>$descuentos,
        ];
        return view('descuento.create',compact('datos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'idCliente'=>'required',
            'tipo_descuento'=>'required',
        ]);
        //dd($request);

        $descuento=Descuento::create($request->except('_token'));
        $descuento->save();

        return redirect()->view('descuento.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
