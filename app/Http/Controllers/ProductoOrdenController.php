<?php

namespace App\Http\Controllers;

use App\EstadoOrdenTrabajo;
use App\ProductoReparacion;
use App\ProductoReparacionOrdenTrabajo;
use Illuminate\Http\Request;

class ProductoOrdenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $productos=ProductoReparacion::all();
        return view('producto.orden',compact('productos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        $producto_orden=ProductoReparacionOrdenTrabajo::create($request->except('_token'));
        $producto_orden->save();
        $esdato=new EstadoOrdenTrabajo;
        $esdato->idOrden=$request->idOrdenTrabajo;
        $esdato->estado='CONFIRMACION_R/P';
        $esdato->save();
        return \redirect()->route('ordentrabajo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
