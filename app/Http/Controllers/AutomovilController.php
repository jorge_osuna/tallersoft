<?php

namespace App\Http\Controllers;

use App\Clientes;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Trasporte;

class AutomovilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $client;
    public function __construct()
    {
        //inicializamos client
        $this->client = new Client(['base_uri'=>'http://veiculos.fipe.org.br/api/veiculos/']);
    }
    public function index()
    {
        //
        $trasportes=Trasporte::all();
        return view('automovil.index',compact('trasportes'));
    }
    public function trasporte_client($id){

        $trasportes=Trasporte::all()->where('idCliente',$id);

    }

    public function car1(){
        $this->client= new Client(['base_uri'=>'http://veiculos.fipe.org.br/api/veiculos/ConsultarMarcas']);
        $response = $this->client->post('ConsultarModelos', [
            'json'=>[
                "codigoTabelaReferencia"=> 231,
                "codigoTipoVeiculo"=> 1,
            ]
        ]);
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //ejecutamos el api
       $response = $this->client->post('ConsultarMarcas', [
            'json'=>[
                "codigoTabelaReferencia"=> 231,
                "codigoTipoVeiculo"=> 1,
            ]
        ]);
        $clientes=Clientes::all();

        return \view('automovil.create',["marcas"=>json_decode($response->getBody(), true)],compact('clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);

        $request->validate([

            'idCliente'=>'required',
            'marca'=>'required',
            'modelo'=>'required',
            'ano'=>'required',
            'no_eco'=>'required',
            'serie'=>'required',
        ]);
        $trasporte=Trasporte::create($request->except('_token'));
        $trasporte->save();
        return \redirect()->route('automovil.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
