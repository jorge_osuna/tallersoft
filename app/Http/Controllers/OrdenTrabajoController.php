<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\EstadoOrdenTrabajo;
use App\Trasporte;
use App\OrdenTrabajo;
use App\TrabajoDefinido;
use Illuminate\Http\Request;
use Intervention\Image\Image;

class OrdenTrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ordenT=OrdenTrabajo::all();
        $estados=[];
        foreach ($ordenT as $orden) {
            array_push($estados,EstadoOrdenTrabajo::latest()->where('idOrden',$orden->id)->first());
        }

        return view('ordentrabajo.index',compact('ordenT','estados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $trabajo=TrabajoDefinido::all();
        $data=[
            'trabajo'=>$trabajo,
        ];

        if($_GET['customRadioInline1']=='carro_chico'){
            return view('ordentrabajo.carro_chico',compact('data'));
        }
        if($_GET['customRadioInline1']=='carro_grande'){
            return view('ordentrabajo.carro_grande',compact('data'));
        }
        if($_GET['customRadioInline1']=='van'){
            return view('ordentrabajo.furgo',compact('data'));
        }
        if($_GET['customRadioInline1']=='trailer'){
            return view('ordentrabajo.trailer',compact('data'));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // \dd($request);
        $img = $_POST['base64'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $fileData = base64_decode($img);
        $fileName = "img/orden_trabajo/".uniqid().'.png';
        
        file_put_contents($fileName, $fileData);
        // \dd($request->all());

        // $request->validate([
        //     'idCliente'=>'required',
        //     'idTrasporte'=>'required',
        //     'idMecanico'=>'required',
        //     'kilometraje'=>'required',
        //     'tanque'=>'required',
        //     'serie_orden_de_trabajo'=>'required',
        // ]);
        //guardar y mover imagen
        $oren=OrdenTrabajo::create($request->except('_token'));
        $oren->base64=$fileName;
        $oren->save();
        $estado=new EstadoOrdenTrabajo;
        $estado->idOrden = $oren->id;
        $estado->estado = 'REGISTRADO';
        $estado->save();
        //dd("orden :",$oren,"estado:",$estado);
        return \redirect()->route('ordentrabajo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $ordenT=OrdenTrabajo::find($id);
        return view('ordentrabajo.show',compact('ordenT'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
