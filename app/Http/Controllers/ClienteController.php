<?php

namespace App\Http\Controllers;

use App\Clientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clientes=Clientes::all();
        return view('Clientes.index',compact('clientes'));
        //return view('Clientes.buscar',compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $enums=explode("','",substr(DB::select('SHOW COLUMNS FROM '."clientes"." LIKE 'forma_pago'")[0]->Type,6,-2));
        return view('Clientes.create',compact('enums'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request);
        //exit();
        $request->validate([

            'nombre' => 'required',
            'rfc' => 'required',
            'forma_pago' => 'required',
            'dirección' => 'required',
            'colonia' => 'required',
            'codigo_postal'=>'integer',
            'ciudad' => 'required',
            'email'=>'required',
            'no_cliente'=>'required',
        ]);

        $clientes=Clientes::create($request->except('_token'));
        $clientes->save();

        return \redirect()->route('Clientes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cliente=Clientes::find($id);
        $enums=explode("','",substr(DB::select('SHOW COLUMNS FROM '."clientes"." LIKE 'forma_pago'")[0]->Type,6,-2));
        $data=[
            'cliente'=>$cliente,
            'enums'=>$enums,
        ];
        return view('clientes.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cliente=Clientes::find($id);
        $cliente->delete();

        return \redirect()->route('Clientes.index');
    }
}
