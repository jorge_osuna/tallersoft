<?php

namespace App\Http\Controllers;

use App\Mecanico;
use Illuminate\Http\Request;

class MecanicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mecanico=Mecanico::all();
        return view('mecanico.index',compact('mecanico'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('mecanico.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //mecanico=Mecanico::all();
        $request->validate([

            'nombre' => 'required',
            'rfc' => 'required',
            'dirección' => 'required',
            'colonia' => 'required',
            'codigo_postal'=>'integer',
            'ciudad' => 'required',
            'email'=>'required',
            'foto_mecanico'=>'required|image|mimes:jpg,png,jpeg',
        ]);

            //guardar y mover imagen
            $carpeta='img/fotos';
            $imagenName=request()->foto_mecanico->getClientOriginalName();
            request()->foto_mecanico->move(public_path($carpeta),$imagenName);
            //dd($imagenName);
            $mecanico=Mecanico::create($request->except('_token'));
            $mecanico->foto_mecanico=$carpeta."/".request()->foto_mecanico->getClientOriginalName();
            //dd($mecanico);
            //dd($request);
            $mecanico->Save();

        return redirect()->route('mecanico.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $mecanico=Mecanico::find($id);
        return view('mecanico.show',compact('mecanico'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $mecanico=Mecanico::find($id);
        $mecanico->delete();
        return \redirect()->route('mecanico.index');
    }
}
