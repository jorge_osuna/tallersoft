<?php

namespace App\Http\Controllers;

use App\TrabajoDefinido;
use Illuminate\Http\Request;

class TrabajoDefinidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $trabajos=TrabajoDefinido::all();
        return view('trabajo.index',compact('trabajos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return \view('trabajo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        $request->validate([
            'nombre'=>'required',
            'descripcion'=>'required',
            'precio'=>'required'
        ]);
            // \dd($request);
        $trabajos=TrabajoDefinido::create($request->except('_token'));
        $trabajos->save();
        $trabajos=TrabajoDefinido::all();
        return \view('trabajo.index',compact('trabajos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $trabajos=TrabajoDefinido::find($id);
        $trabajos->delete();
        $trabajos=TrabajoDefinido::all();
        return  view('trabajo.index',compact('trabajos'));
    }
}
