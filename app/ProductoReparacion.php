<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Almacen;
use App\ProductoReparacionOrdenTrabajo;
class ProductoReparacion extends Model
{
    //
    protected $fillable = [
        'codigo_barra','nombre','descripcion','tipo_unidad','costo_compra','costo_venta','cantidad'
    ];

    public function almacen(){
        return $this->belongsToMany(Almacen::class);
    }
    public function productoReparacionOrdenT(){
        return $this->belongsToMany(ProductoReparacionOrdenTrabajo::class);
    }
}
