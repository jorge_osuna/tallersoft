<?php

namespace App;
use App\ProductoReparacionOrdenTrabajo;
use App\RefaccionOrdenTrabajo;
use App\TrabajoDefinidoOrdenTrabajo;
use App\EstadoOrdenTrabajo;
use App\Mecanico;
use App\Trasporte;
use App\Clientes;

use Illuminate\Database\Eloquent\Model;

class OrdenTrabajo extends Model
{
    //
    protected $fillable = [
        'idCliente','tipo_orden','idTrasporte','esterio','cb','antena_cb','barras_cont','vcr',
        'monitores','maleteros','ceniceros','funda_asientos','serie_orden_de_trabajo',
        'fechaEmision','FechaPrometida','idEstadoOrden','reporteInicial','obcervaciones','idMecanico',
        'kilometraje'
    ];

    public function productoReparacionOrdenT(){
        return $this->belongsToMany(ProductoReparacionOrdenTrabajo::class);
    }
    public function refaccionOrdenT(){
        return $this->belongsToMany(RefaccionOrdenTrabajo::class);
    }
    public function trabajoDefinidoOrdenT(){
        return $this->belongsToMany(TrabajoDefinidoOrdenTrabajo::class);
    }
    public function estadoOrdenT(){
        return $this->hasMany(EstadoOrdenTrabajo::class);
    }
    public function mecanico(){
        return $this->hasMany(Mecanico::class);
    }
    public function trasporte(){
        return $this->hasMany(Trasporte::class);
    }
    public function cliente(){
        return $this->hasMany(Clientes::class);
    }
}
