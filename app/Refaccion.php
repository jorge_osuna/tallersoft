<?php

namespace App;

use App\Almacen;
use App\RefaccionOrdenTrabajo;
use Illuminate\Database\Eloquent\Model;

class Refaccion extends Model
{
    //
    protected $fillable = [
        'codigo_barra','nombre','descripcion','modelo','tipo_unidad','costo_compra','costo_venta','cantidad'
    ];

    public function almacen(){
        return $this->belongsToMany(Almacen::class);
    }
    public function refaccionesOrdenT(){
        return $this->belongsToMany(RefaccionOrdenTrabajo::class);
    }
}
