<?php

namespace App;
use App\OrdenTrabajo;
use Illuminate\Database\Eloquent\Model;

class EstadoOrdenTrabajo extends Model
{
    //
    protected $fillable = [
        'idOrden','estado'
    ];
    public function ordenTrabajo(){
        return $this->belongsToMany(OrdenTrabajo::class);
    }
}
