<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('clientes')->insert([
            'nombre'=>'Alejandro',
            'apellido_p'=>'placido',
            'apellido_m'=>'lopez',
            'rfc'=>'palo940702ly7',
            'forma_pago'=>'EFECTIVO',
            'dirreccion'=>'emiliano zapata # 95',
            'colonia'=>'palo verde',
            'codigo_postla'=>'83280',
            'ciudad'=>'hermosillo',
            'email'=>'alejandro@hotmail.com',
            'no_cliente'=>'hmo234',
        ]);
    }
}
