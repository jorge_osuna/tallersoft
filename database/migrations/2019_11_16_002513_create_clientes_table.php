<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',45);
            $table->string('apellido_p',45)->nullable();
            $table->string('apellido_m',45)->nullable();
            $table->string('rfc',13);
            $table->enum('forma_pago',array('EFECTIVO','CHEQUE','TRASFERENCIA_ELECTRÓNICA_DE_FONDOS','TARJETA_DE_CRÉDITO','MONEDERO_ELECTRONICO','DINERO_ELECTRÓNICO','VALES_DE_DESPENSA','TARJETA_DE_DÉBITO','TARJETA_DE_SERVICIO','OTROS'));
            $table->string('dirección',255)->nullable();
            $table->string('colonia',100);
            $table->string('codigo_postal',45);
            $table->string('ciudad',45);
            $table->integer('telefono')->nullable();
            $table->integer('celular')->nullable();
            $table->string('email',45);
            $table->text('observaciones')->nullable();
            $table->string('no_cliente',15)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
