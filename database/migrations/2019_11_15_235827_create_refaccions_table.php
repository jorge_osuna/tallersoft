<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefaccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refaccions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo_barra',255);
            $table->string('nombre',45);
            $table->text('descripcion');
            $table->string('modelo',45);
            $table->string('tipo_unidad',45);
            $table->integer('cantidad')->nullable();
            $table->double('costo_compra');
            $table->double('costo_venta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refaccions');
    }
}
