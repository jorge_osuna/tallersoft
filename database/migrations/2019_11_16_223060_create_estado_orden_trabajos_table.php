<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadoOrdenTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado_orden_trabajos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idOrden')->unsigned()->nullable();
            $table->foreign('idOrden')->references('id')->on('orden_trabajos');
            $table->enum('estado',array('REGISTRADO', 'EN_COLA', 'OBSERVACION', 'CONFIRMACION_R/P', 'EN_ESPERA_R/P', 'R/P_EN_TALLER', 'EN_TRABJO', 'TERMINADO', 'PAGADO_FACTURADO', 'ENTREGADO', 'FINALIZADO'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estado_orden_trabajos');
    }
}
