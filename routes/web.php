<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Clientes;
use App\Mecanico;
use App\Trasporte;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    if(Auth::user()){
        return view('home');
    }
    return view('auth.login');
});
Route::get('opciones', function () {
    $mecanicos=Mecanico::all();
    $clientes=Clientes::all();
    $trasportes=Trasporte::all();
    $data=[
        'trasportes'=>$trasportes,
        'mecanicos'=>$mecanicos,
        'clientes'=>$clientes
    ];
    return view('ordentrabajo.create',compact('data'));
});
Route::resource('produ_orden', 'ProductoOrdenController');
Route::resource('refa_orden', 'RefaccionOrdenController');
Route::resource('almacen', 'AlmacenController');
Route::resource('/Clientes', 'ClienteController');
Route::resource('/herramientas', 'HerramientasTallerController');
Route::resource('/producto', 'ProductoReparacionController');
Route::resource('/refaccion', 'RefaccionController');
Route::resource('/trabajo', 'TrabajoDefinidoController');
Route::resource('/descuento', 'DescuentoController');
Route::resource('/automovil', 'AutomovilController');
Route::resource('/mecanico', 'MecanicoController');
Route::resource('/ordentrabajo', 'OrdenTrabajoController');
Route::resource('/estado', 'EstadoController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('users/{id}', function ($id) {
    
});
