@extends('layouts.app')
@section('content')
<form action="{{ Route('ordentrabajo.create') }}" method="get" name="etp1">
    <div class="container-fluid px-15 mx-15">
        <div class="row">
            <div class="col-xs-7 col-sm-7">
                <div class="form-group">
                    {{-- Mecanico --}}
                    <div class="row pb-4">
                        <div class="col-8">
                            <label for="idMecanico">Mecanico</label>
                            <input type="text" class="form-control" name="idMecanico" id="idMecanico" placeholder="Mecanico" value="" required>
                        </div>
                        <div class="col-4 align-self-center pt-4">
                            <a name="buscar" id="buscar" class="btn btn-primary" href="{{Route('mecanico.index')}}" target="_blank" role="button">Buscar</a>
                        </div>
                    </div>
                    {{-- Cliente --}}
                    <div class="row pb-4">
                        <div class="col-8">
                            <label for="idCliente">Cliente</label>
                            <input type="text" class="form-control" name="idCliente" id="idCliente" placeholder="Cliente" value="" required>
                        </div>
                        <div class="col-4 align-self-center pt-4">
                            <a name="buscar" id="buscar" class="btn btn-primary" href="{{Route('Clientes.index')}}" target="_blank" role="button">Buscar</a>
                        </div>
                    </div>
                    {{-- Trasporte --}}
                    <div class="row pb-4">
                        <div class="col-8">
                            <label for="idTrasporte">Trasporte</label>
                            <input type="text" class="form-control" name="idTrasporte" id="idTrasporte" placeholder="Trasporte" value="" required>
                        </div>
                        <div class="col-4 align-self-center pt-4">
                            <a name="buscar" id="buscar" class="btn btn-primary" href="{{Route('automovil.index')}}" target="_blank" role="button">Buscar</a>
                        </div>
                    </div>
                    {{-- Select de Tipo De Trasporte --}}
                    <div class="row pb-4 ">
                        <div class="col-xs-6 pb-4">
                            <div class="custom-control custom-radio custom-control-inline">
                                <div class="card border-info">
                                  <img class="card-img-top img-thumbnail" src="{{asset('img/carroChico.jpg')}}" alt="carro chico"
                                    style="width:150px;height:150px">
                                    <div class="card-body">
                                        <input type="radio" id="carroChico" name="customRadioInline1" class="custom-control-input" value="carro_chico" required>
                                        <label class="custom-control-label" for="carroChico">Carro Chico</label>
                                    </div>
                                </div>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <div class="card border-info">
                                    <img class="card-img-top img-thumbnail" src="{{asset('img/camioneta.jpg')}}" alt="carro grande"
                                    style="width:150px;height:150px">
                                    <div class="card-body">
                                        <input type="radio" id="carroGrande" name="customRadioInline1" class="custom-control-input" value="carro_grande" required>
                                        <label class="custom-control-label" for="carroGrande">Carro Grande</label>
                                    </div>
                                </div>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <div class="card border-info">
                                    <img class="card-img-top img-thumbnail" src="{{asset('img/furgoneta.jpg')}}" alt="van"
                                    style="width:150px;height:150px">
                                    <div class="card-body">
                                        <input type="radio" id="furgoneta" name="customRadioInline1" class="custom-control-input" value="van" required>
                                        <label class="custom-control-label" for="furgoneta">Furgoneta</label>
                                    </div>
                                </div>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <div class="card border-info">
                                <img class="card-img-top img-thumbnail" src="{{asset('img/trailer.jpg')}}" alt="trailes"
                                style="width:150px;height:150px">
                                <div class="card-body">
                                    <input type="radio" id="camion" name="customRadioInline1" class="custom-control-input" value="trailer" required>
                                    <label class="custom-control-label" for="camion">camion</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-5 col-ms-5">
            {{-- Tabla de Mostrar Mecanico --}}
            <p>
                <a class="btn btn-primary" data-toggle="collapse" href="#mostraMeca" aria-expanded="false" aria-controls="mostraMeca">
                    Mecanicos.....
                </a>
            </p>
            <div class="collapse" id="mostraMeca">
                <table class="table table-hover table-inverse table-responsive">
                    <thead class="thead-inverse">
                        <tr>
                            <th>id</th>
                            <th>Nombre</th>
                            <th>Apellido P</th>
                            <th>Apellido M</th>
                            <th>Opcion</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['mecanicos'] as $mecanico)
                                <tr>
                                    <td scope="row">{{  $mecanico->id }}</td>
                                    <td>{{  $mecanico->nombre }}</td>
                                    <td>{{  $mecanico->apellido_p }}</td>
                                    <td>{{  $mecanico->apellido_m }}</td>
                                    <td>
                                        <button type="button" name="" id="mecanico_{{  $mecanico->id }}" class="btn btn-info btn-sm btn-block" onclick="selccionarMeca(this.id);">Select</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
            {{-- Tabla de Mostrar Cliente --}}
            <p>
                <a class="btn btn-primary" data-toggle="collapse" href="#mostraCliente" aria-expanded="false" aria-controls="mostraCliente">
                    Clientes.....
                </a>
            </p>
            <div class="collapse" id="mostraCliente">
                <table class="table table-hover table-inverse table-responsive">
                    <thead class="thead-inverse">
                        <tr>
                            <th>id</th>
                            <th>Nombre</th>
                            <th>Apellido P</th>
                            <th>Apellido M</th>
                            <th>Opcion</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['clientes'] as $cliente)
                                <tr>
                                    <td scope="row">{{  $cliente->id }}</td>
                                    <td>{{  $cliente->nombre }}</td>
                                    <td>{{  $cliente->apellido_p }}</td>
                                    <td>{{  $cliente->apellido_m }}</td>
                                    <td>
                                        <button type="button" name="" id="cliente_{{  $cliente->id }}" class="btn btn-info btn-sm btn-block" onclick="selccionarCliente(this.id);">Select</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
            {{-- Tabla de Mostrar Trasporte --}}
            <p>
                <a class="btn btn-primary" data-toggle="collapse" href="#mostraTrasporte" aria-expanded="false" aria-controls="mostraTrasporte">
                    Trasporte.....
                </a>
            </p>
            <div class="collapse" id="mostraTrasporte">
                <table class="table table-hover table-inverse table-responsive">
                    <thead class="thead-inverse">
                        <tr>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Año</th>
                            <th>Serie</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['trasportes'] as $trasporte)
                                <tr>
                                    <td scope="row">{{  $trasporte->id }}</td>
                                    <td>{{  $trasporte->marca }}</td>
                                    <td>{{  $trasporte->modelo }}</td>
                                    <td>{{  $trasporte->año }}</td>
                                    <td>
                                        <button type="button" name="" id="trasporte_{{  $trasporte->id }}" class="btn btn-info btn-sm btn-block" onclick="selccionarTrasporte(this.id);">Select</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="container">
        {{-- btn Formulario --}}
        <div class="row p-15">
            <div class="col-xs-6">
                <button type="submit" class="btn btn-info">Seleccionar</button>
            </div>
        </div>
    </div>
</form>
@endsection
