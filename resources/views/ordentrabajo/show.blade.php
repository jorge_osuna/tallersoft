@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-2 col-sm-2 align-items-center">
            <div class="form-group">
                <label for="idMecanico">Mecanico: </label>
                <input type="text" class="form-control" name="idMecanico" id="idMecanico" value='{{$ordenT->idMecanico}}' onkeydown="event.preventDefault();">
            </div>
        </div>
        <div class="col-xl-2 col-sm-2 align-items-center">
            <div class="form-group">
                <label for="idCliente">Cliente :</label>
                <input type="text" class="form-control" name="idCliente" id="idCliente" value="{{$ordenT->idCliente}}"  onkeydown="event.preventDefault();">
            </div>
        </div>
        <div class="col-xl-2 col-sm-2 align-items-center">
            <div class="form-group">
                <label for="idTrasporte">Trasporte :</label>
                <input type="text" class="form-control" name="idTrasporte" id="idTrasporte" value="{{$ordenT->idTrasporte}}" onkeydown="event.preventDefault();">
            </div>
        </div>
    </div>
    <div class="row">
        <!--checkBox-->
        <div class="col-xl-5 col-sm-5 align-items-center">
            <label for="">Normal</label>
        </div>
        <div class="col-xl-5 col-sm-5 align-self-end">
            <label for="serie_orden_de_trabajo">Serie Orden Trabajo</label>
            <input type="text" name="serie_orden_de_trabajo" id="serie_orden_de_trabajo" value="{{ $ordenT->serie_orden_de_trabajo }}" onkeydown="event.preventDefault();">
        </div>
    </div>
</div>
<!--Canvas-->
<div class="container">
    <div class="row d-flex">
        <div class="col-xl-6 col-xs-12">
            <img src="{{asset($ordenT->base64)}}" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="imagen del canvas">
        </div>
        <!--CheckList-->
        <div class="col-xl-6 col-xs-12">
            <div id="checkList" role="tablist" aria-multiselectable="true">
                {{-- <label for="">{{dd($ordenT)}}</label> --}}
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="esterio" name="esterio"
                                    @if ($ordenT->esterio=='on')
                                        checked
                                    @endif>
                                    <label class="custom-control-label" for="esterio">Esterio</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="cb" name="cb"
                                     @if ($ordenT->cb=='on')
                                        checked
                                    @endif>
                                    <label class="custom-control-label" for="cb">CB</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="antena_cb" name="antena_cb"
                                    @if ($ordenT->antena_cb=='on')
                                        checked
                                    @endif>
                                    <label class="custom-control-label" for="antena_cb">Antena CB</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="barras_cont" name="barras_cont"
                                    @if ($ordenT->barras_cont=='on')
                                        checked
                                    @endif
                                    >
                                    <label class="custom-control-label" for="barras_cont">Barras Cont</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="vcr" name="vcr"
                                    @if ($ordenT->vcr=='on')
                                        checked
                                    @endif  
                                    >
                                    <label class="custom-control-label" for="vcr">VCR</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="monitores" name="monitores" 
                                    @if ($ordenT->monitores=='on')
                                     checked
                                    @endif  
                                    >
                                    <label class="custom-control-label" for="monitores">monitores</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="ceniceros" name="ceniceros"
                                    @if ($ordenT->ceniceros=='on')
                                     checked
                                    @endif
                                    >
                                    <label class="custom-control-label" for="ceniceros">Ceniceros</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="funda_asientos" name="funda_asientos"
                                    @if ($ordenT->funda_asientos=='on')
                                     checked
                                    @endif>
                                    <label class="custom-control-label" for="funda_asientos">Funda Asientos</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6 pt-2">
                    <div class="form-group">
                        <input type="text" class="form-control" name="kilometraje" id="kilometraje" value="{{$ordenT->kilometraje}}">
                    </div>
                </div>
                <div class="col-6">
                    <img src="{{ asset('img/tanque/T'.$ordenT->tanque.'.jpg') }}" alt="tanque" usemap="#tanque" id="imgtanque">
                </div>
            </div>
            <div class="row pt-4">
                <div class="col-12">
                    <label for="fechaEmsion">Fecha Emision : {{$ordenT->fechaEmision}}</label>
                </div>
                <div class="col-12">
                    <label for="fechaPrometida">Fecha Prometida : {{$ordenT->fechaPrometida}}</label>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                      <label for="reporteInicial">Reporte Inicial</label>
                      <textarea class="form-control" name="reporteInicial" id="reporteInicial" rows="4">{{$ordenT->reporteInicial}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="obcervaciones">Observaciones</label>
                        <textarea class="form-control" name="obcervaciones" id="obcervaciones" rows="4">{{$ordenT->obcervaciones}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
