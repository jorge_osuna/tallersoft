@extends('layouts.app')
@section('content')
<div class="container tarlogin">
    <form action="{{ Route('descuento.store')}}" method="post">
        @csrf
        <div class="row">
            <div class="col-xl-4">
                <div class="form-group">
                <label for="idCliente">Cliente</label>
                <select class="form-control form-control-xs" name="idCliente" id="idCliente">
                    @foreach ($datos['clientes'] as $cliente)
                        <option value="{{ $cliente->id }}">{{ $cliente->nombre }}    {{ $cliente->rfc}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="form-group">
                    <label for="tipo_descuento">Tipo Descuento</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">%</div>
                        </div>
                        <input type="text" class="form-control" name="tipo_descuento" id="tipo_descuento" placeholder="cantidad de porciento de descuento">
                    </div>
                </div>
            </div>
            <div class="col-xl-4 align-self-center pt-3">
                <button type="submit" class="btn btn-info">Agregar</button>
            </div>
        </div>
    </form>
</div>
@endsection
