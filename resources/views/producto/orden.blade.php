@extends('layouts.app')
@section('content')
<div class="container">
    <form action="{{route('produ_orden.store')}}" method="post">
        @csrf
        <?php
            $id=$_GET['idOrdenTrabajo'];
        ?>
        <input type="hidden" name="idOrdenTrabajo" value="{{$id}}">
        <div class="row">
            <div class="col-xl-1-4">
                <div class="form-group">
                  <label for="idProductoReparacion">Producto</label>
                  <select class="form-control" name="idProductoReparacion" id="idProductoReparacion">
                    @foreach ($productos as $item)
                        <option value="{{$item->id}}">{{$item->nombre}}---{{$item->descripcion}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="col-xl-4-7">
                <div class="form-group">
                  <label for="cantidad">Cantidad</label>
                  <input type="number"
                    class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad">
                </div>
            </div>
            <div class="col-xl-7-12 p-4">
                <button type="submit" class="btn btn-info">Agregar</button>
            </div>
        </div>
    </form>
</div>
@endsection
