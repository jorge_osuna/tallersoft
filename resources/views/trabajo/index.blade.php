@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="container tarlogin">
        <div class="row">
            <div class="col-12 aling-right">
                <button class="btn" type="button" data-toggle="collapse"
                    data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                    style="width: 50px; height: 30px;" >
                        <img class="card-img-top" src="{{ asset('img/minimiza.svg')}}" alt="">
                </button>
            </div>
        </div>
        <form action="" method="post">
            @csrf
            <div class="row" id="collapseExample">
                <div class="col-4">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text"
                        class="form-control" name="nombre" id="nombre" placeholder="Nombre del trabajo">
                    </div>
                </div>
                <div class="col-3 align-self-center pt-3">
                    <button type="submit" class="btn btn-light">Buscar</button>
                </div>
            </div>
        </form>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <a class="btn btn-light border border-primary" href="{{Route('trabajo.create')}}" role="button">Nuevo Cliente</a>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <table class="table table-hover table-inverse table-responsive">
            <thead class="thead-inverse">
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                    <th>Eliminar</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($trabajos as $trabajo)             
                        <tr>
                            <td>{{$trabajo->nombre}}</td>
                            <td>{{$trabajo->descripcion}}</td>
                            <td>{{$trabajo->precio}}</td>
                            <td>
                                <form action="{{ Route('trabajo.destroy',$trabajo->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-warning btn-sm">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
</div>
@endsection
