@extends('layouts.app')
@section('content')
<div class="container-fluid">
        <div class="container tarlogin">
            <div class="row">
                <div class="col-12 aling-right">
                    <button class="btn" type="button" data-toggle="collapse"
                     data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                     style="width: 50px; height: 30px;" >
                          <img class="card-img-top" src="{{ asset('img/minimiza.svg')}}" alt="">
                    </button>
                </div>
            </div>
            <form action="" method="post">
                @csrf
                <div class="row" id="collapseExample">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text"
                            class="form-control" name="nombre" id="nombre" placeholder="Nombre">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apellido">
                        </div>
                    </div>
                    <div class="col-3 align-self-center pt-3">
                        <button type="submit" class="btn btn-light">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <a class="btn btn-light border border-primary" href="{{Route('mecanico.create')}}" role="button">Nuevo Mecanico</a>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <table class="table table-hover table-inverse table-responsive">
                <thead class="thead-inverse">
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido P</th>
                        <th>Apellido M</th>
                        <th rowspan="2">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($mecanico as $mecani)
                            <tr>
                                <td>{{ $mecani->nombre }}</td>
                                <td>{{ $mecani->apellido_p }}</td>
                                <td>{{ $mecani->apellido_m }}</td>
                                <td>
                                    <form action="{{ Route('mecanico.show',$mecani->id) }}" method="get">
                                        @csrf
                                        <input type="text" name="id_mecanico" id="id_mecanico" class="d-none" value="{{$mecani->id}}">
                                        <input type="submit" value="Ver" class="btn-sm btn btn-info">
                                    </form>
                                </td>
                                <td>
                                    <form action="{{ Route('mecanico.destroy',$mecani->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Eliminar" class="btn-sm btn btn-danger">
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
            </table>
        </div>
    </div>
@endsection
