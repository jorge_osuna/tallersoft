@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="" method="post">
            <div class="form-group">
                <div class="row">
                    <div class="col-4">
                        <label for="rfc">RFC </label>
                        <input type="text" name="rfc" id="rfc" class="form-control" placeholder="RFC" value="{{ $data['cliente']->rfc }}" disabled>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="forma_pago">Metodo Pago</label>
                            <select class="custom-select" name="forma_pago" id="forma_pago">
                                <option selected>{{ $data['cliente']->forma_pago }}</option>
                                @foreach ($data['enums'] as $enum)
                                    <option>{{$enum}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <label for="nombre">Nombre </label>
                        <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre" value="{{ $data['cliente']->nombre }}" >
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <label for="apellido_p">Apellido P</label>
                        <input type="text" name="apellido_p" id="apellido_p" class="form-control" placeholder="Apellido Paterno" value="{{ $data['cliente']->apellido_p }}">
                    </div>
                    <div class="col-6">
                        <label for="apellido_m">Apellido M</label>
                        <input type="text" name="apellido_m" id="apellido_m" class="form-control" placeholder="Apellido Materno" value="{{ $data['cliente']->apellido_m }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label for="dirección">Dirección *</label>
                        <input type="text" name="dirección" id="dirección" class="form-control" placeholder="Dirección" value="{{ $data['cliente']->dirección }}" >
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label for="colonia">Colonia  </label>
                        <input type="text" name="colonia" id="colonia" class="form-control" placeholder="Direccion" value="{{ $data['cliente']->colonia }}" >
                    </div>
                    <div class="col-4">
                        <label for="codigo_postal">Codigo Postal</label>
                        <input type="text" name="codigo_postal" id="codigo_postal" class="form-control" placeholder="codigo postal" value="{{ $data['cliente']->codigo_postal }}">
                    </div>
                    <div class="col-4">
                        <label for="ciudad">Ciudad </label>
                        <input type="text" name="ciudad" id="ciudad" class="form-control" placeholder="Ciudad" value="{{ $data['cliente']->ciudad }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label for="telefono">Telefono</label>
                        <input type="text"class="form-control" name="telefono" id="telefono" placeholder="Telefono" value="{{ $data['cliente']->telefono }}">
                    </div>
                    <div class="col-4">
                        <label for="movil">Movil</label>
                        <input type="text"class="form-control" name="movil" id="movil" placeholder="celular" value="{{ $data['cliente']->movil }}">
                    </div>
                    <div class="col-4">
                        <label for="email">Email </label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Correo Electronico" value="{{ $data['cliente']->email }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="observaciones">Observaciones</label>
                            <textarea class="form-control" name="observaciones" id="observaciones" rows="5">{{ $data['cliente']->observaciones }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-info" disabled>Modificar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
