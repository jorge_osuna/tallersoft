@extends('layouts.app')
@section('content')
<div class="container-fluid">
        <div class="container tarlogin">
            <div class="row">
                <div class="col-12 aling-right">
                    <button class="btn" type="button" data-toggle="collapse"
                     data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                     style="width: 50px; height: 30px;" >
                          <img class="card-img-top" src="{{ asset('img/minimiza.svg')}}" alt="">
                    </button>
                </div>
            </div>
            <form action="" method="post">
                @csrf
                <div class="row" id="collapseExample">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="no_cliente">No.Cliente</label>
                            <input type="text"
                            class="form-control" name="no_cliente" id="no_cliente" placeholder="Numero Cliente">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                        <label for="rfc">tipo</label>
                        <input type="text" class="form-control" name="tipo" id="tipo" placeholder="RFC">
                        </div>
                    </div>
                    <div class="col-3 align-self-center pt-3">
                        <button type="submit" class="btn btn-light">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <a class="btn btn-light border border-primary" href="{{Route('automovil.create')}}" role="button">Nuevo Automovil</a>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <table class="table table-hover table-inverse table-responsive">
                <thead class="thead-inverse">
                    <tr>
                        <th>NO.Cliente</th>
                        {{-- <th>Nombre</th> --}}
                        <th>Modelo</th>
                        <th>Marca</th>
                        <th>Año</th>
                        <th>No.Eco</th>
                        <th>Serie</th>
                        <th rowspan="2">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($trasportes as $trasporte)
                            <tr>
                                <td>{{ $trasporte->idCliente }}</td>
                                {{-- <td>{{$trasporte->}}</td> --}}
                                <td>{{ $trasporte->marca }}</td>
                                <td>{{ $trasporte->modelo }}</td>
                                <td>{{ $trasporte->ano }}</td>
                                <td>{{ $trasporte->no_eco }}</td>
                                <td>{{ $trasporte->serie }}</td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
@endsection
